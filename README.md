# Levit's Django CookieCutter template with Bootstrap Sass (and more)

### Before getting started

This cookie cutter provides a Django project built according to the _project app_ architecture.

In short, with the _project app_ architecture, the almost empty directory named after your project becomes an *app* like any other app in your project. This has numerous advantages, the most noticeable of them being that you don't need to create a _core_ or _base_ app but can use your project app instead.

If you haven't seen [Anatomy of a Django Project by Mark Lavin](https://www.youtube.com/watch?v=ajEDo1semzs), I would emcourage you to do so to get a better grasp of what the _project app_ architecture is about.

### CookieCutter

For more info on CookieCutter, please visit [their documentation](http://cookiecutter.readthedocs.org/en/latest/)

### Usage

To use, simply run 
`cookiecutter https://bitbucket.org/levit_scs/cc_project_app_full_with_hooks.git`

### Requirements:

- [CookieCutter](http://cookiecutter.readthedocs.org/en/latest/)
- wget
- unzip
- [The compass compiler](http://compass-style.org/help/) (in order to compile sass source files into css). Compass is easily installable via several sources like ruby-gem or npm.

### Included in this CoookieCutter:

- [Django](https://www.djangoproject.com/)
- [Crispy forms](http://django-crispy-forms.readthedocs.org/en/latest/)
- [Celery](http://www.celeryproject.org/)
- [Django Debug Toolbar](http://django-debug-toolbar.readthedocs.org/en/1.4/) and [Django Debug Toolbar Template timings](https://github.com/orf/django-debug-toolbar-template-timings)
- [Django Webtest](https://github.com/django-webtest/django-webtest)
- [Factory Boy](https://factoryboy.readthedocs.org/en/latest/)

- [Bootstrap 3 (Sass Source)](http://getbootstrap.com/)

### Notes:

1. Make sure your system allows you to run scripts from /tmp (this isn't the case for a default Ubuntu installation)

2. **This template currently only works on unix-based systems. PR for an MS Windows .bat hook or for converting the current hook to a python hook are welcome**


---


This project is licensed under the [MIT License](http://opensource.org/licenses/MIT)

Before contributing, commenting or interacting with this project in any form, please, make sure you read and understand our [Code of Conduct](COC.md)