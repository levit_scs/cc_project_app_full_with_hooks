#!/bin/bash
USER="{{ cookiecutter.username }}"
EMAIL="{{ cookiecutter.email }}"
virtualenv -p {{ cookiecutter.python }} venv
source venv/bin/activate
pip install --upgrade pip
pip install -r requirements.txt
cp environ.py.dist environ.py
python manage.py migrate --noinput
{% if cookiecutter.create_superuser == 'yes' %}
  echo "Creating superuper {{ cookiecutter.username }}"
  python manage.py createsuperuser --username {{ cookiecutter.username }} --email {{ cookiecutter.email }}
{% endif %}
wget https://github.com/twbs/bootstrap-sass/archive/v{{ cookiecutter.bootstrap_version }}.tar.gz
tar -xzf v{{ cookiecutter.bootstrap_version }}.tar.gz
mkdir -p {{ cookiecutter.repo_name}}/static/js
mv bootstrap-sass-{{ cookiecutter.bootstrap_version }}/assets/javascripts/* {{ cookiecutter.repo_name}}/static/js/
mv bootstrap-sass-{{ cookiecutter.bootstrap_version }}/assets/fonts {{ cookiecutter.repo_name}}/static/
mv bootstrap-sass-{{ cookiecutter.bootstrap_version }}/assets/stylesheets/* sass/css/
rm v{{ cookiecutter.bootstrap_version }}.tar.gz
rm -rf bootstrap-sass-{{ cookiecutter.bootstrap_version }}
wget -O {{ cookiecutter.repo_name }}/static/js/jquery.min.js http://code.jquery.com/jquery-{{ cookiecutter.jquery_version}}.min.js
compass compile

